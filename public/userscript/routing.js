var userapp=angular.module('userApp', ['ng-webcam','ui.router','angularFromUI','angularEditFromUI','angularUtils.directives.dirPagination','ui.bootstrap', 'ui.bootstrap.datetimepicker','datepicker']);
userapp.run(function(userprojectsService, userallmytimesheetsService,userapprovedtimesheetsService, userpendingtimesheetsService,employeeService,$rootScope){

function setUserID(){
if($rootScope.currentemployeeid){
userservice.setEmpID($rootScope.currentemployeeid)
}};

function getuserprojectsJsonConfig() {
              userprojectsService.getuserprojectsJsonConfig().then(function (resultDetails) {
              userprojectsService.setuserprojectsFromConfig(resultDetails.data)
              }, function error(errResponse) {
               console.log("cannot get settings config")
               		})
               	}
function getuserallmytimesheetsJsonConfig() {
                userallmytimesheetsService.getuserallmytimesheetsJsonConfig().then(function (resultDetails) {
                userallmytimesheetsService.setuserallmytimesheetsFromConfig(resultDetails.data)
                }, function error(errResponse) {
                 console.log("cannot get settings config")
                 		})
                 	}
function getuserapprovedtimesheetsJsonConfig() {
                userapprovedtimesheetsService.getuserapprovedtimesheetsJsonConfig().then(function (resultDetails) {
                userapprovedtimesheetsService.setuserapprovedtimesheetsFromConfig(resultDetails.data)
                }, function error(errResponse) {
                 console.log("cannot get settings config")
                 		})
                 	}

function getuserpendingtimesheetsJsonConfig() {
                userpendingtimesheetsService.getuserpendingtimesheetsJsonConfig().then(function (resultDetails) {
                userpendingtimesheetsService.setuserpendingtimesheetsFromConfig(resultDetails.data)
                }, function error(errResponse) {
                 console.log("cannot get settings config")
                 		})
                 	}
function getemployeeDetailsJsonConfig() {
            employeeService.getemployeeJsonConfig().then(function (resultDetails) {
                employeeService.setemployeeFromConfig(resultDetails.data)
            }, function error(errResponse) {
                console.log("cannot get settings config")
            })
        }


 $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if (sessionStorage.restorestate == "true") {
                $rootScope.$broadcast('restorestate'); //let everything know we need to restore state
                sessionStorage.restorestate = false;
            }
        });

        //let everthing know that we need to save state now.
        window.onbeforeunload = function (event) {
            $rootScope.$broadcast('savestate');
        };



function init(){
   getuserprojectsJsonConfig();
   getuserallmytimesheetsJsonConfig();
   getuserapprovedtimesheetsJsonConfig();
   getuserpendingtimesheetsJsonConfig();
   getemployeeDetailsJsonConfig();
   setUserID();


}
 init();
});


userapp.config(function($stateProvider, $urlRouterProvider) {
$urlRouterProvider.otherwise('/login');


$stateProvider
.state('login', {
               url: '/login',
                templateUrl: 'user/login.html',
                     controller:'loginController'

        })


$stateProvider
    .state('dashboard', {
      url: "/dashboard",
              templateUrl: 'user/dashboard.html',
              controller:'dashboardCtrl'

            })

$stateProvider
    .state('basicinformation',{
	url:"/basicinformation",
	templateUrl: 'user/basicInformation.html'
                        })


$stateProvider
    .state('admindetails',{
	url:"/admindetails",
	templateUrl:'user/admindetails.html',
	controller:'employeeCtrl'

})

$stateProvider
    .state('userdetails',{
	url:"/usedertails",
	templateUrl:'user/userdetails.html',
	controller:'employeeCtrl'
})


$stateProvider
    .state('userdetailsedit',{
	url:"/userdetailsedit",
	templateUrl:'user/userdetailsedit.html',
	controller:'employeeCtrl'
})





$stateProvider
    .state('projectinformation',{
	url:"/projectinformation",
	templateUrl:'user/projectinformation.html',
	abstract:true
})

$stateProvider
    .state('projectinformation.userprojects',{
	url:"/userprojects",
	templateUrl:'user/userprojects.html',
	controller:'userprojectsCtrl'
})

$stateProvider
    .state('projectinformation.projectmeetingfollowup',{
	url:"/projectmeetingfollowup",
	templateUrl:'user/projectmeetingfollowup.html',
	controller:'userprojectsCtrl'
})


$stateProvider
    .state('usertimesheets',{
	url:"/usertimesheets",
	templateUrl: 'user/usertimesheet.html'
})
$stateProvider
    .state('usertimesheets.userallmytimesheets',{
	url:"/userallmytimesheets",
	templateUrl:'user/userallmytimesheets.html',
	controller:'userallmytimesheetsCtrl'
})
$stateProvider
    .state('usertimesheets.userapprovedtimesheets',{
	url:"/userapprovedtimesheets",
	templateUrl:'user/userapprovedtimesheets.html',
	controller:'userallmytimesheetsCtrl'
})
$stateProvider
    .state('usertimesheets.userpendingtimesheets',{
	url:"/userpendingtimesheets",
	templateUrl:'user/userpendingtimesheets.html',
	controller:'userallmytimesheetsCtrl'
})


});



angular.module('userApp').directive('exportTable', function () {
    var link = function ($scope, element, attr) {
            $scope.$on('export-pdf',function(e,d){
            element.exportDetails({ type:'pdf', escape: false });
            })

            $scope.$on('export-excel', function(e,d){
                element.exportDetails({ type:'excel', escape: false });
            });
            $scope.$on('export-doc',function(e,d){
                element. exportDetails({ type:'doc', escape: false});
            });
            $scope.$on('export-csv', function(e,d){
                element. exportDetails({ type:'csv', escape:false });
            });
    }
    return {
        restrict:'A',
        link:link
    };
});

